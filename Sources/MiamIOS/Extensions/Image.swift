//
//  Image.swift
//  MiamIOSFramework
//
//  Created by Vincent Kergonna on 26/07/2022.
//

import Foundation
import SwiftUI

@available(iOS 14, *)
extension Image {
    static func miamImage(icon: MiamIcon) -> Image {
        guard let image = UIImage(named: icon.rawValue) else {
            return Image(icon.rawValue, bundle: Bundle.miamBundle)
        }

        return Image(uiImage: image)
    }
    
    init(packageResource name: String, ofType type: String) {
            #if canImport(UIKit)
            guard let path = Bundle.miamBundle.path(forResource: name, ofType: type),
                  let image = UIImage(contentsOfFile: path) else {
                self.init(name)
                return
            }
            self.init(uiImage: image)
            #elseif canImport(AppKit)
            guard let path = Bundle.miamBundle.path(forResource: name, ofType: type),
                  let image = NSImage(contentsOfFile: path) else {
                self.init(name)
                return
            }
            self.init(nsImage: image)
            #else
            self.init(name)
            #endif
        }
}
