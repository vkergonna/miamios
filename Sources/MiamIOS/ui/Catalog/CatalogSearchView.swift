//
//  CatalogSearchView.swift
//  MiamIOSFramework
//
//  Created by Vincent Kergonna on 24/06/2022.
//

import SwiftUI
import miamCore

@available(iOS 14, *)
struct CatalogSearchView: View {
    @SwiftUI.State var searchString: String = ""
    var catalog: CatalogVM
    let close: () -> Void
    let search: () -> Void
    var body: some View {
        if let template = Template.sharedInstance.catalogSearchViewTemplate {
            template(catalog, close, search)
        } else {
            VStack(spacing: 10.0) {
                HStack {
                    Button {
                        close()
                    } label: {
                        Image.miamImage(icon: .arrow)
                    }
                    Spacer()
                }.frame(height: 44).padding(Dimension.sharedInstance.mlPadding)
                HStack(spacing: 10.0) {
                    HStack(spacing: 10.0) {
                        TextField(MiamText.sharedInstance.search, text: $searchString).frame(height: 45.0)
                            .disableAutocorrection(true)
                            .onChange(of: searchString) { value in
                                if let filtersViewModel = catalog.filtersViewModel {
                                    filtersViewModel.setSearchString(searchString: value)
                                }
                            }
                        Button {
                            search()
                        } label: {
                            Image.miamImage(icon: .search)
                                .padding(10)
                                .background(Color.miamColor(.primary)).clipShape(Circle())
                        }
                    }.padding([.leading], 15).frame(height: 45.0)
                        .overlay(Capsule().stroke(Color.gray, lineWidth: 1.0))
                }.padding(10)
                Spacer()
            }
        }
    }
}

@available(iOS 14, *)
struct CatalogSearchView_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            CatalogSearchView(catalog: CatalogVM(), close: {}, search: {})
        }
    }
}
