//
//  Text.swift
//  MiamIOSFramework
//
//  Created by Miam on 27/04/2022.
//

import Foundation


public class MiamText {
    
    public static let sharedInstance = MiamText()

    public var addRecipe = "Découvrir la recette"
    public var alreadyInCart = "Voir les produits"
    public var cookTime = "Temps de cuisson"
    public var checkBasketPreview = "Ajouter les ingrédients"
    public var currency = "€"
    public var ingredients = "ingredients"
    public var preGuests = "par personne"
    public var recipeFlag = "Idée repas"
    public var replaceBy = "Remplacer cet article par : "
    public var preparation = "Préparation"
    public var totalTime = "Temps total"
    public let recpeitDetailsInfo = "Plus d'informations"
    public let mealRowAlready = "Déjà dans vos placards"
    public let mealRowNotFound = "Produits indisponibles"
    public let mealRowRemoved = "Articles retirés du panier"
    public let select = "Sélectionner"
    public let swapProduct = "Remplacer le produit"
    public var difficultyEasy = "Chef débutant"
    public var difficultyMid = "Chef aguerrie"
    public var difficultyHard = "Top Chef"
    public var preparationTime = "Préparation"
    public var cookingTime = "Cuisson"
    public var restingTime = "Repos"
    public var simmering = "Ça mijote"
    public var viewRecipeDetail = "Voir le détail"
    public var selectedProduct = "Dans le panier"
    public var steps = "Étapes"
    public var browseRecipesText = "Parcourir les idées repas"
    public var noRecipeFoundText = "Oups, aucune recette n'a été trouvée"
    public var tryAnotherSearchText = "Essayez une nouvelle recherche"
    public var addRecipeText = "Ajouter une idée repas"
    
    public var filtersTitle = "Affiner ma sélection"
    public var filtersDifficultySectionTitle = "Difficulté"
    public var filterCostSectionTitle = "Coût par personne"
    public var filterPreparationTimeSectionTitle = "Temps de préparation"
    public var removeFiltersButtonTitle = "Retirer les filtres"
    
    public var noFavoritRecipeYet = "Oups, vous n'avez pas encore d'idée repas"
    public var search = "Rechercher"
    public var showAll = "Tout voir"
    public var mealIdeas = "Idées repas en 1 clic"
    public var myMealIdeas = "Mes idées repas"
   
    public var noMealIdeaInBasket = "Vous n'avez aucune idée repas dans votre panier."
    public var addIngredientText = "Ajouter"
    public var showDetails = "Voir le détail"
   
    public var removeFromBasket = "Retirer du panier"
    public var keepShopping = "Continuer mes achats"
    
    public var moreInformation = "Plus d'infos"
    public var replaceIngredient = "Remplacer"
   
    public var persons = "pers."
    public var numberOfPersons = "Nombre de personnes"
    
    public var mealsAdded = "repas ajouté"
    public var mealsAddedPlural = "repas ajoutés"
    public var cancel = "Annuler"
    public var see = "Voir les"
    public var meals = "repas"
    public var addTag = "Ajouter +"
   
    public var searchTagPlaceholder = "Rechercher un ingrédient"
  
    public var preferencesTitle = "Mes préférences"
    public var preferencesSearchTitle = "Je n'aime pas"
    public var dietTitle = "Régime particulier"
    public var dietSubtitle = "Avez-vous un régime particulier ?"
    public var cookingModesTitle = "Mode de cuisson"
    public var cookingModesSubtitle = "De quels modes de cuissons disposez-vous ?"
    public var tastesTitle = "Goûts"
    public var tastesSubtitle = "Y a-t-il des ingrédients que vous n'aimez pas ?"
    
    private init(){}
}
