Pod::Spec.new do |spec|

  spec.name         = "MiamIOS"
  spec.version      = "3.0.1"
  spec.summary      = "Miam SDK for iOS"
  spec.description  = <<-DESC
  Miam SDK for iOS.
                   DESC

  spec.homepage     = "https://www.miam.tech"
  spec.license      = "GPLv3"
  spec.license      = { :type => "GPLv3", :file => "LICENSE" }
  spec.author             = { "Vincent Kergonna" => "it@miam.tech" }
  spec.platform     = :ios, "11.0"
  spec.source       = { :git => "https://gitlab.com/miam/kmm-miam-sdk", :tag => "#{spec.version}" }
  spec.source_files  = "Sources/**/*.{h,c,cpp,swift}", "changelog.md"
  spec.exclude_files = "Classes/Exclude"
  spec.resource_bundles = {
    'MiamIOS_MiamIOS' => [ # Match the name SPM Generates
       'Sources/MiamIOS/Resources/miam.xcassets'
    ]
  }
  spec.resource  = "MiamIOSFramework/miam.xcassets"
  spec.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  spec.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  spec.vendored_frameworks = "miamCore.xcframework"

end
